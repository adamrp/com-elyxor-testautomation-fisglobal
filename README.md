### Setup

Requires Chrome browser installed on host running test suite. 

### Running Tests

To run suite.  This defaults to run any test in com.elyxor.testautomation.test package
```
$ gradle clean test
```

To override the package used, pass in the package as an argument
```
-PtestPackage=com.elyxor.testautomation.packagename
```
