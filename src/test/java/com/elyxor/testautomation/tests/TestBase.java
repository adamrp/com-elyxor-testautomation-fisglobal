package com.elyxor.testautomation.tests;

import com.elyxor.testautomation.utils.TimeUtils;
import com.elyxor.testautomation.utils.driver.Browser;
import com.elyxor.testautomation.utils.driver.Driver;
import com.elyxor.testautomation.configuration.TestAutomationConfiguration;
import com.elyxor.testautomation.utils.driver.WebDriverFactory;
import com.elyxor.testautomation.utils.testdata.TestData;
import com.elyxor.testautomation.utils.testdata.TestDataFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;


public class TestBase {

    private static TestData testData;
    private Driver driver;
    protected static final int DEFAULT_PAUSE_TEST_TIME_IN_SECONDS = 5;

    private static final Logger logger = LoggerFactory.getLogger(TestBase.class);

    @BeforeSuite
    public void beforeSuite() {
        TestDataFactory tdf = new TestDataFactory();
        testData = tdf.getTestData();
    }

    @BeforeClass
    public void beforeClass() throws Exception {
        TestAutomationConfiguration configInstance = TestAutomationConfiguration.getInstance();
        Browser browser = Browser.getByName(configInstance.getValue("test.browser.name"));
        WebDriverFactory wdf = new WebDriverFactory();
        driver = wdf.getDriver(browser);
    }

    @AfterClass
    public void afterClass() {
        driver.closeDriver();
    }

    public String getTestData(String testCase, String key) {
        return this.testData.get(testCase, key);
    }

    public void printTestCaseData(String testCase) { this.testData.printTestCaseData(testCase);}

    public Driver getDriver() {
        return this.driver;
    }

    public void pauseTest(Integer waitTimeSeconds) {
        logger.info(String.format("Pausing Test for %s seconds.", waitTimeSeconds));
        TimeUtils.waitForSeconds(waitTimeSeconds);
    }

    public void pauseTest() {
        Integer waitTimeSeconds = DEFAULT_PAUSE_TEST_TIME_IN_SECONDS;
        logger.info(String.format("Pausing Test for %s seconds.", waitTimeSeconds));
        TimeUtils.waitForSeconds(waitTimeSeconds);
    }
}
