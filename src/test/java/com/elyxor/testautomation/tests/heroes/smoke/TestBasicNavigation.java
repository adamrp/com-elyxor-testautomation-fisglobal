package com.elyxor.testautomation.tests.heroes.smoke;

import com.elyxor.testautomation.pages.heroes.HeroesDashboardPage;
import com.elyxor.testautomation.pages.heroes.HeroesHeroesPage;
import com.elyxor.testautomation.tests.TestBase;
import org.openqa.selenium.WebDriverException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestBasicNavigation extends TestBase {

  private static final Logger logger = LoggerFactory.getLogger(TestBasicNavigation.class);
  private String testCase = this.getClass().getSimpleName();

  HeroesDashboardPage heroesDashboardPage;
  HeroesHeroesPage heroesHeroesPage;

  @Test(enabled = true)
  public void basicNavigation() {

    printTestCaseData(testCase);
    heroesDashboardPage = new HeroesDashboardPage();

    Assert.assertEquals(heroesDashboardPage.getTextLblTopHeroes(),
        getTestData(testCase, "topheroes"));

    heroesHeroesPage = heroesDashboardPage.clickBtnHeroes();

    Assert.assertEquals(heroesHeroesPage.getTextLblMyHeroes(),
        getTestData(testCase, "myheroes"));
    Assert.assertNotNull(heroesHeroesPage.getTextLstHeroServiceMessages());
    pauseTest();
    heroesHeroesPage.clickBtnClear();
    pauseTest();

    Assert.assertEquals(heroesHeroesPage.getTextLstHeroServiceMessages(), "");

  }
}
