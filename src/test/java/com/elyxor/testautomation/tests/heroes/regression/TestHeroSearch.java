package com.elyxor.testautomation.tests.heroes.regression;

import com.elyxor.testautomation.pages.heroes.HeroesDashboardPage;
import com.elyxor.testautomation.pages.heroes.HeroesDetailPage;
import com.elyxor.testautomation.tests.TestBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestHeroSearch extends TestBase {

  private static final Logger logger = LoggerFactory.getLogger(TestHeroSearch.class);
  private String testCase = this.getClass().getSimpleName();

  HeroesDashboardPage heroesDashboardPage;
  HeroesDetailPage heroesDetailPage;

  @Test(enabled = true)
  public void heroSearch() {

    printTestCaseData(testCase);
    heroesDashboardPage = new HeroesDashboardPage();

    heroesDashboardPage.setTextHeroSearch(getTestData(testCase, "search"));
    Assert.assertTrue(heroesDashboardPage.getTextBtnHeroSearchResults()
        .contains(getTestData(testCase, "name")));

    heroesDetailPage = heroesDashboardPage.clickBtnHeroInSearchResults(getTestData(testCase, "name"));

    Assert.assertEquals(heroesDetailPage.getTextLblHeroDetails(),
        getTestData(testCase, "details"));

  }
}
