package com.elyxor.testautomation.tests.heroes.regression;

import com.elyxor.testautomation.pages.heroes.HeroesDashboardPage;
import com.elyxor.testautomation.pages.heroes.HeroesDetailPage;
import com.elyxor.testautomation.tests.TestBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestEditHero extends TestBase {

  private static final Logger logger = LoggerFactory.getLogger(TestEditHero.class);
  private String testCase = this.getClass().getSimpleName();

  HeroesDashboardPage heroesDashboardPage;
  HeroesDetailPage heroesDetailPage;

  @Test(enabled = true)
  public void editHero() {

    printTestCaseData(testCase);
    heroesDashboardPage = new HeroesDashboardPage();

    Assert.assertTrue(
        heroesDashboardPage.getTextLstTopHeroes().contains(getTestData(testCase, "hero")));

    heroesDetailPage = heroesDashboardPage.clickBtnTopHero(getTestData(testCase, "hero"));

    heroesDetailPage.setTextTxtHeroName(getTestData(testCase, "edit"));
    heroesDetailPage.clickBtnSave();
    Assert.assertTrue(
        heroesDashboardPage.getTextLstTopHeroes().contains(getTestData(testCase, "edited")));

  }
}
