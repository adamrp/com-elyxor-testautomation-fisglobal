package com.elyxor.testautomation.tests.heroes.regression;

import com.elyxor.testautomation.pages.heroes.HeroesDashboardPage;
import com.elyxor.testautomation.pages.heroes.HeroesHeroesPage;
import com.elyxor.testautomation.tests.TestBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestFailedAssertion extends TestBase {

  private static final Logger logger = LoggerFactory.getLogger(TestFailedAssertion.class);
  private String testCase = this.getClass().getSimpleName();

  HeroesDashboardPage heroesDashboardPage;
  HeroesHeroesPage heroesHeroesPage;

  @Test(enabled = true)
  public void failedAssertion() {

    printTestCaseData(testCase);
    heroesDashboardPage = new HeroesDashboardPage();

    Assert.assertEquals(heroesDashboardPage.getTextLstTopHero(), getTestData(testCase, "badhero"));

    heroesHeroesPage = heroesDashboardPage.clickBtnHeroes();

    Assert
        .assertTrue(heroesHeroesPage.getTextLstHeroes().contains(getTestData(testCase, "hero")));

  }

}
