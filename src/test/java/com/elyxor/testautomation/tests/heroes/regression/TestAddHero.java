package com.elyxor.testautomation.tests.heroes.regression;

import com.elyxor.testautomation.pages.heroes.HeroesDashboardPage;
import com.elyxor.testautomation.pages.heroes.HeroesHeroesPage;
import com.elyxor.testautomation.tests.TestBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestAddHero extends TestBase {

  private static final Logger logger = LoggerFactory.getLogger(TestAddHero.class);
  private String testCase = this.getClass().getSimpleName();

  HeroesDashboardPage heroesDashboardPage;
  HeroesHeroesPage heroesHeroesPage;

  @Test(enabled = true)
  public void addHero() {

    printTestCaseData(testCase);
    heroesDashboardPage = new HeroesDashboardPage();

    heroesHeroesPage = heroesDashboardPage.clickBtnHeroes();
    heroesHeroesPage.setTextTxtHeroName(getTestData(testCase, "hero"));
    heroesHeroesPage.clickBtnAdd();

    pauseTest();
    Assert.assertTrue(
        heroesHeroesPage.getTextLstHeroes().contains(getTestData(testCase, "hero")));

  }
}
