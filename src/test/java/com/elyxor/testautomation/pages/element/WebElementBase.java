package com.elyxor.testautomation.pages.element;

import com.elyxor.testautomation.configuration.TestAutomationConfiguration;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WebElementBase {

    private WebDriver driver;
    private String id;

    protected By locator;
    protected WebDriverWait wait;

    public WebElementBase(WebDriver driver, By locator) {
        this.driver = driver;
        this.locator = locator;

        TestAutomationConfiguration configInstance = TestAutomationConfiguration.getInstance();
        Integer webdriverwait = Integer.parseInt(configInstance.getValue("webdriver.webdriverwait"));
        this.wait = new WebDriverWait(driver, webdriverwait);
    }

    protected WebElement element(){
        wait.until(ExpectedConditions.presenceOfElementLocated(locator));
        return driver.findElement(locator);
    }

}
