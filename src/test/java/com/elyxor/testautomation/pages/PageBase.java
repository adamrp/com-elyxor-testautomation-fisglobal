package com.elyxor.testautomation.pages;

import com.elyxor.testautomation.configuration.TestAutomationConfiguration;
import com.elyxor.testautomation.utils.TimeUtils;
import com.elyxor.testautomation.utils.driver.Driver;
import java.util.Iterator;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class PageBase {

  protected static final int WAIT_FOR_PAGE_OPEN_IN_SECONDS = 5;
  private WebDriverWait webDriverWait;

  protected PageBase() {
    openPage();
    PageFactory.initElements(Driver.getWebDriver(), this);
    waitForPageOpen();
  }

  // on the Page, define what openPage means.  This often is just nothing
  protected abstract void openPage();

  // on the Page, define what is means to be opened.  This often is waiting for element to be present
  public abstract boolean isPageOpened();

  protected String getTitle() {
    return Driver.getWebDriver().getTitle();
  }

  protected void waitForPageOpen() {
    int secondsCount = 0;
    boolean isPageOpenedIndicator = isPageOpened();
    while (!isPageOpenedIndicator && secondsCount < WAIT_FOR_PAGE_OPEN_IN_SECONDS) {
      TimeUtils.waitForSeconds(1);
      secondsCount++;
      isPageOpenedIndicator = isPageOpened();
    }
    if (!isPageOpened()) {
      throw new AssertionError("Page did not open.");
    }
  }

  protected boolean waitForObject(By by) {
    // use this method on pages that require find element calls

    if (webDriverWait == null) {
      TestAutomationConfiguration configInstance = TestAutomationConfiguration.getInstance();
      Integer webdriverwait = Integer.parseInt(configInstance.getValue("webdriver.webdriverwait"));
      this.webDriverWait = new WebDriverWait(Driver.getWebDriver(), webdriverwait);
    }

    try {
      webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(by));
      return true;
    } catch (Exception e) {
      return false;
    }
  }

  protected String getTextListItem(List<WebElement> webElements, boolean allItems) {
    if (allItems) {
      StringBuilder sb = new StringBuilder();
      Iterator<WebElement> itr = webElements.iterator();
      while (itr.hasNext()) {
        sb.append(itr.next().getText());
        if (itr.hasNext()) {
          sb.append(("\r\n"));
        }
      }
      return sb.toString();
    }else {
      return webElements.get(webElements.size() - 1).getText();
    }
  }

  protected String getTextListItem(List<WebElement> webElements, int itemPosition) {
    return webElements.get(itemPosition - 1).getText();
  }

  protected void clickListItem(List<WebElement> webElements, String searchText) {
    for (WebElement we : webElements) {
      if (we.getText().contains(searchText)) {
        we.click();
        return;
      }
    }
  }

  protected void clickListItem(List<WebElement> webElements, int itemPosition) {
    webElements.get(itemPosition - 1).click();
  }

  protected void clickListItem(List<WebElement> webElements) {
    webElements.get(webElements.size() - 1).click();
  }

  protected boolean waitForObjectToBePresent(By by) {
      // use this method on pages that require find element calls

      if (webDriverWait == null) {
          TestAutomationConfiguration configInstance = TestAutomationConfiguration.getInstance();
          Integer webdriverwait = Integer.parseInt(configInstance.getValue("webdriver.webdriverwait"));
          this.webDriverWait = new WebDriverWait(Driver.getWebDriver(), webdriverwait);
      }

      try
      {
          webDriverWait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(by));
          return true;
      }
      catch (Exception e)
      {
          return false;
      }
  }

  protected boolean waitForObjectToBeVisible(By by) {
    // use this method on pages that require find element calls

    if (webDriverWait == null) {
      TestAutomationConfiguration configInstance = TestAutomationConfiguration.getInstance();
      Integer webdriverwait = Integer.parseInt(configInstance.getValue("webdriver.webdriverwait"));
      this.webDriverWait = new WebDriverWait(Driver.getWebDriver(), webdriverwait);
    }

    try
    {
      webDriverWait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(by));
      return true;
    }
    catch (Exception e)
    {
      return false;
    }
  }

}
