package com.elyxor.testautomation.pages.account.opening;

import com.elyxor.testautomation.pages.PageBase;
import java.util.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ManualEntryContinuedPage extends PageBase {

  private static final Logger logger = LoggerFactory.getLogger(ManualEntryContinuedPage.class);

  private static final String BTN_BACK_XPATH = "";
  private static final String BTN_BACKUPWITHHOLDINGNO_XPATH = "";
  private static final String BTN_BACKUPWITHHOLDINGYES_XPATH = "";
  private static final String BTN_CANCEL_XPATH = "";
  private static final String BTN_CONTINUE_XPATH = "";
  private static final String BTN_LIVEDTWOYEARSNO_XPATH = "";
  private static final String BTN_LIVEDTWOYEARSYES_XPATH = "";
  private static final String BTN_SAMEASPRIMARNO_XPATH = "";
  private static final String BTN_SAMEASPRIMARYES_XPATH = "";
  private static final String CHK_CERTIFYINFORMATION_XPATH = "";
  private static final String DDL_COUNTRY_XPATH = "";
  private static final String DDL_EMPLOYEMENTSTATUS_XPATH = "";
  private static final String DDL_STATE_XPATH = "";
  private static final String DP_DATEOFBIRTH_XPATH = "";
  private static final String LBL_PAGEHEADING_XPATH = "";
  private static final String LNK_BACKUPWITHOLDING_XPATH = "";
  private static final String LNK_FATCAREPORTING_XPATH = "";
  private static final String LNK_SOCIALSECURITYNUMBER_XPATH = "";
  private static final String LNK_USPERSON_XPATH = "";
  private static final String TXT_CITY_XPATH = "";
  private static final String TXT_PRIMARYADDRESS_XPATH = "";
  private static final String TXT_PRIMARYADDRESSOPTIONAL_XPATH = "";
  private static final String TXT_TAXIDENTIFICATIONNUMBER_XPATH = "";
  private static final String TXT_ZIPCODE_XPATH = "";

  @FindBy(xpath = BTN_BACK_XPATH)
  @CacheLookup
  WebElement btnBack;

  @FindBy(xpath = BTN_BACKUPWITHHOLDINGNO_XPATH)
  @CacheLookup
  WebElement btnBackupWithholdingNo;

  @FindBy(xpath = BTN_BACKUPWITHHOLDINGYES_XPATH)
  @CacheLookup
  WebElement btnBackupWithholdingYes;

  @FindBy(xpath = BTN_CANCEL_XPATH)
  @CacheLookup
  WebElement btnCancel;

  @FindBy(xpath = BTN_CONTINUE_XPATH)
  @CacheLookup
  WebElement btnContinue;

  @FindBy(xpath = BTN_LIVEDTWOYEARSNO_XPATH)
  WebElement btnLivedTwoYearsNo;

  @FindBy(xpath = BTN_LIVEDTWOYEARSYES_XPATH)
  WebElement btnLivedTwoYearsYes;

  @FindBy(xpath = BTN_SAMEASPRIMARNO_XPATH)
  WebElement btnSameAsPrimaryNo;

  @FindBy(xpath = BTN_SAMEASPRIMARYES_XPATH)
  WebElement btnSameAsPrimaryYes;

  @FindBy(xpath = CHK_CERTIFYINFORMATION_XPATH)
  @CacheLookup
  WebElement chkCertifyInformation;

  @FindBy(xpath = DDL_COUNTRY_XPATH)
  @CacheLookup
  WebElement ddlCountry;

  @FindBy(xpath = DDL_EMPLOYEMENTSTATUS_XPATH)
  @CacheLookup
  WebElement ddlEmploymentStatus;

  @FindBy(xpath = DDL_STATE_XPATH)
  @CacheLookup
  WebElement ddlState;

  @FindBy(xpath = DP_DATEOFBIRTH_XPATH)
  @CacheLookup
  WebElement dpDateOfBirth;

  @FindBy(xpath = LBL_PAGEHEADING_XPATH)
  @CacheLookup
  WebElement lblPageHeading;

  @FindBy(xpath = LNK_BACKUPWITHOLDING_XPATH)
  @CacheLookup
  WebElement lnkBackupWithholding;

  @FindBy(xpath = LNK_FATCAREPORTING_XPATH)
  @CacheLookup
  WebElement lnkFATCAReporting;

  @FindBy(xpath = LNK_SOCIALSECURITYNUMBER_XPATH)
  @CacheLookup
  WebElement lnkSocialSecurityNumber;

  @FindBy(xpath = LNK_USPERSON_XPATH)
  @CacheLookup
  WebElement lnkUSPerson;

  @FindBy(xpath = TXT_CITY_XPATH)
  @CacheLookup
  WebElement txtCity;

  @FindBy(xpath = TXT_PRIMARYADDRESS_XPATH)
  @CacheLookup
  WebElement txtPrimaryAddress;

  @FindBy(xpath = TXT_PRIMARYADDRESSOPTIONAL_XPATH)
  @CacheLookup
  WebElement txtPrimaryAddressOptional;

  @FindBy(xpath = TXT_TAXIDENTIFICATIONNUMBER_XPATH)
  @CacheLookup
  WebElement txtTaxIdentificationNumber;

  @FindBy(xpath = TXT_ZIPCODE_XPATH)
  @CacheLookup
  WebElement txtZipCode;

  public ManualEntryContinuedPage() {
    super();
  }

  @Override
  protected void openPage() {
    //Do nothing.
  }

  @Override
  public boolean isPageOpened() {
    try {
      return lblPageHeading.isDisplayed();
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  public String getTextBackButton() {
    return btnBack.getText();
  }

  public void clickBackupWithholdingNoButton(){
    btnBackupWithholdingNo.click();
  }

  public void clickBackupWithholdingYesButton(){
    btnBackupWithholdingYes.click();
  }

  public String getTextCancelButton() {
    return btnCancel.getText();
  }

  public String getTextContinueButton() {
    return btnContinue.getText();
  }

  public DriversLicensePage clickContinueButton() {
    btnContinue.click();
    return new DriversLicensePage();
  }

  public String getTextLivedTwoYearsNoButton() {
    return btnLivedTwoYearsNo.getText();
  }

  public void clickLivedTwoYearsNoButton() {
    btnLivedTwoYearsNo.click();
  }

  public String getTextLivedTwoYearsYesButton() {
    return btnLivedTwoYearsYes.getText();
  }

  public void clickLivedTwoYearsYesButton() {
    btnLivedTwoYearsYes.click();
  }

  public String getTextSameAsPrimaryNoButton() {
    return btnSameAsPrimaryNo.getText();
  }

  public void clickSameAsPrimaryNoButton() {
    btnSameAsPrimaryNo.click();
  }

  public String getTextSameAsPrimaryYesButton() {
    return btnSameAsPrimaryYes.getText();
  }

  public void clickSameAsPrimaryYesButton() {
    btnSameAsPrimaryYes.click();
  }

  public void clickCertifyInformationCheckbox(){
    chkCertifyInformation.click();
  }

  public String getTextCountry() {
    return ddlCountry.getText();
  }

  public void selectCountry(String country) {
    ddlCountry.sendKeys(country);
  }

  public String getTextEmploymentStatus(){
    return ddlEmploymentStatus.getText();
  }

  public void selectEmploymentStatus(String status){
    ddlEmploymentStatus.sendKeys(status);
  }

  public String getTextState() {
    return ddlState.getText();
  }

  public void selectState(String state) {
    ddlState.sendKeys(state);
  }

  public String getTextDateOfBirth() {
    return dpDateOfBirth.getText();
  }

  public void selectDateOfBirth(String dateOfBirth) {
    dpDateOfBirth.sendKeys(dateOfBirth);
  }

  public String getTextPageHeading() {
    return lblPageHeading.getText();
  }

  public String getTextBackupWithholdingLink(){
    return lnkBackupWithholding.getText();
  }

  public String getTextFATCAReportingLink(){
    return lnkFATCAReporting.getText();
  }

  public String getTextSocialSecurityNumberLink(){
    return lnkSocialSecurityNumber.getText();
  }

  public String getTextUSPersonLink(){
    return lnkUSPerson.getText();
  }

  public String getTextCity() {
    return txtCity.getText();
  }

  public void setTextCity(String city) {
    txtCity.sendKeys(city);
  }

  public String getTextPrimaryAddress() {
    return txtPrimaryAddress.getText();
  }

  public void setTextPrimaryAddress(String address) {
    txtPrimaryAddress.sendKeys(address);
  }

  public String getTextPrimaryAddressOptional() {
    return txtPrimaryAddressOptional.getText();
  }

  public void setTextPrimaryAddressOptional(String address) {
    txtPrimaryAddressOptional.sendKeys(address);
  }

  public String getTextTaxIdentificationNumber(){
    return txtTaxIdentificationNumber.getText();
  }

  public void setTextTaxIdentificationNumber(String taxIdentificationNumber){
    txtTaxIdentificationNumber.sendKeys(taxIdentificationNumber);
  }

  public String getTextZipCode() {
    return txtZipCode.getText();
  }

  public void setTextZipCode(String zipCode) {
    txtZipCode.sendKeys(zipCode);
  }
}
