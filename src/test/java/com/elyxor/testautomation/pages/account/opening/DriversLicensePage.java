package com.elyxor.testautomation.pages.account.opening;

import com.elyxor.testautomation.pages.PageBase;
import java.util.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DriversLicensePage extends PageBase {

  private static final Logger logger = LoggerFactory.getLogger(DriversLicensePage.class);

  private static final String BTN_BACK_XPATH = "";
  private static final String BTN_CANCEL_XPATH = "";
  private static final String BTN_CONTINUE_XPATH = "";
  private static final String DDL_STATEOFISSUE_XPATH = "";
  private static final String DP_EXPIRATIONDATE_XPATH = "";
  private static final String DP_ISSUEDATE_XPATH = "";
  private static final String LBL_PAGEHEADING_XPATH = "";
  private static final String TXT_DRIVERSLICENSENUMBER_XPATH = "";


  @FindBy(xpath = BTN_BACK_XPATH)
  @CacheLookup
  WebElement btnBack;

  @FindBy(xpath = BTN_CANCEL_XPATH)
  @CacheLookup
  WebElement btnCancel;

  @FindBy(xpath = BTN_CONTINUE_XPATH)
  @CacheLookup
  WebElement btnContinue;

  @FindBy(xpath = DDL_STATEOFISSUE_XPATH)
  WebElement ddlStateOfIssue;

  @FindBy(xpath = DP_EXPIRATIONDATE_XPATH)
  WebElement dpExpirationDate;

  @FindBy(xpath = DP_ISSUEDATE_XPATH)
  WebElement dpIssueDate;

  @FindBy(xpath = LBL_PAGEHEADING_XPATH)
  @CacheLookup
  WebElement lblPageHeading;

  @FindBy(xpath = TXT_DRIVERSLICENSENUMBER_XPATH)
  @CacheLookup
  WebElement txtDriversLicenseNumber;

  public DriversLicensePage() {
    super();
  }

  @Override
  protected void openPage() {
    //Do nothing.
  }

  @Override
  public boolean isPageOpened() {
    try {
      return lblPageHeading.isDisplayed();
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  public String getTextBackButton() {
    return btnBack.getText();
  }

  public String getTextCancelButton() {
    return btnCancel.getText();
  }

  public String getTextContinueButton() {
    return btnContinue.getText();
  }

  public TermsAndConditionsPage clickContinueButton() {
    btnContinue.click();
    return new TermsAndConditionsPage();
  }

  public String getTextStateOfIssue() {
    return ddlStateOfIssue.getText();
  }

  public void selectStateOfIssue(String state){
    ddlStateOfIssue.sendKeys(state);
  }

  public String getTextExpirationDate(){
    return dpExpirationDate.getText();
  }

  public void selectExpirationDate(String date){
    dpExpirationDate.sendKeys(date);
  }

  public String getTextIssueDate(){
    return dpIssueDate.getText();
  }

  public void selectIssueDate(String date){
    dpIssueDate.sendKeys(date);
  }

  public String getTextPageHeading() {
    return lblPageHeading.getText();
  }

  public String getTextDriversLicenseNumber() {
    return txtDriversLicenseNumber.getText();
  }

  public void setDriversLicenseNumber(String licenseNumber) {
    txtDriversLicenseNumber.sendKeys(licenseNumber);
  }

}
