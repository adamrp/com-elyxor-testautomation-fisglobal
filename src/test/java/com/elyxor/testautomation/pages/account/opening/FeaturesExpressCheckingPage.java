package com.elyxor.testautomation.pages.account.opening;

import com.elyxor.testautomation.pages.PageBase;
import com.sun.net.httpserver.Authenticator.Success;
import java.util.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FeaturesExpressCheckingPage extends PageBase {

  private static final Logger logger = LoggerFactory.getLogger(FeaturesExpressCheckingPage.class);

  private static final String BTN_BACK_XPATH = "";
  private static final String BTN_CANCEL_XPATH = "";
  private static final String BTN_CONTINUE_XPATH = "";
  private static final String CHK_DEBITCARD_XPATH = "";
  private static final String CHK_ONLINEBILLPAYMENT_XPATH = "";
  private static final String LBL_PAGEHEADING_XPATH = "";
  private static final String LNK_MAKESELECTION_XPATH = "";
  private static final String LNK_OVERDRAFTPROTECTION_XPATH = "";

  @FindBy(xpath = BTN_BACK_XPATH)
  @CacheLookup
  WebElement btnBack;

  @FindBy(xpath = BTN_CANCEL_XPATH)
  @CacheLookup
  WebElement btnCancel;

  @FindBy(xpath = BTN_CONTINUE_XPATH)
  @CacheLookup
  WebElement btnContinue;

  @FindBy(xpath = CHK_DEBITCARD_XPATH)
  WebElement chkDebitCard;

  @FindBy(xpath = CHK_ONLINEBILLPAYMENT_XPATH)
  WebElement chkOnlineBillPayment;

  @FindBy(xpath = LBL_PAGEHEADING_XPATH)
  @CacheLookup
  WebElement lblPageHeading;

  @FindBy(xpath = LNK_MAKESELECTION_XPATH)
  @CacheLookup
  WebElement lnkMakeSelection;

  @FindBy(xpath = LNK_OVERDRAFTPROTECTION_XPATH)
  @CacheLookup
  WebElement lnkOverdraftProtection;

  public FeaturesExpressCheckingPage() {
    super();
  }

  @Override
  protected void openPage() {
    //Do nothing.
  }

  @Override
  public boolean isPageOpened() {
    try {
      return lblPageHeading.isDisplayed();
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  public String getTextBackButton() {
    return btnBack.getText();
  }

  public String getTextCancelButton() {
    return btnCancel.getText();
  }

  public String getTextContinueButton() {
    return btnContinue.getText();
  }

  public SuccessPage clickContinueButton() {
    btnContinue.click();
    return new SuccessPage();
  }

  public String getTextDebitCardCheckbox(){
    return chkDebitCard.getText();
  }

  public void clickDebitCardCheckbox(){
    chkDebitCard.click();
  }

  public String getTextOnlineBillPaymentCheckbox(){
    return chkOnlineBillPayment.getText();
  }

  public void clickTextOnlineBillPaymentCheckbox(){
    chkOnlineBillPayment.click();
  }

  public String getTextPageHeading() {
    return lblPageHeading.getText();
  }

  public String getTextMakeSelectionLink(){
    return lnkMakeSelection.getText();
  }

  public void clickTextMakeSelectionLink(){
    lnkMakeSelection.click();
  }

  public String getTextOverDraftProtectionLink(){
    return lnkOverdraftProtection.getText();
  }

  public void clickTextOverDraftProtectionLink(){
    lnkOverdraftProtection.click();
  }
}
