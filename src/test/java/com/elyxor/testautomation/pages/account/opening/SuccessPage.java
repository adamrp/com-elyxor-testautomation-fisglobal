package com.elyxor.testautomation.pages.account.opening;

import com.elyxor.testautomation.pages.PageBase;
import java.util.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SuccessPage extends PageBase {

  private static final Logger logger = LoggerFactory.getLogger(SuccessPage.class);

  private static final String BTN_DONE_XPATH = "";
  private static final String BTN_ENROLL_XPATH = "";
  private static final String LBL_PAGEHEADING_XPATH = "";

  @FindBy(xpath = BTN_DONE_XPATH)
  @CacheLookup
  WebElement btnDone;

  @FindBy(xpath = BTN_ENROLL_XPATH)
  @CacheLookup
  WebElement btnEnroll;

  @FindBy(xpath = LBL_PAGEHEADING_XPATH)
  @CacheLookup
  WebElement lblPageHeading;

  public SuccessPage() {
    super();
  }

  @Override
  protected void openPage() {
    //Do nothing.
  }

  @Override
  public boolean isPageOpened() {
    try {
      return lblPageHeading.isDisplayed();
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  public String getTextDoneButton() {
    return btnDone.getText();
  }

  public void clickDoneButton() {
    btnDone.click();
  }

  public String getTextEnrollButton() {
    return btnEnroll.getText();
  }

  public void clickEnrollButton() {
    btnEnroll.click();
  }

  public String getTextPageHeading() {
    return lblPageHeading.getText();
  }

}
