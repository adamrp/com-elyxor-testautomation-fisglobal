package com.elyxor.testautomation.pages.account.opening;

import com.elyxor.testautomation.pages.PageBase;
import java.util.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CreditCardOfferPage extends PageBase {

  private static final Logger logger = LoggerFactory.getLogger(CreditCardOfferPage.class);

  private static final String BTN_NO_XPATH = "";
  private static final String BTN_YES_XPATH = "";
  private static final String LBL_OFFERTEXT_XPATH = "";
  private static final String LBL_PAGEHEADING_XPATH = "";

  @FindBy(xpath = BTN_NO_XPATH)
  @CacheLookup
  WebElement btnNo;

  @FindBy(xpath = BTN_YES_XPATH)
  @CacheLookup
  WebElement btnYes;

  @FindBy(xpath = LBL_OFFERTEXT_XPATH)
  @CacheLookup
  WebElement lblOfferText;

  @FindBy(xpath = LBL_PAGEHEADING_XPATH)
  @CacheLookup
  WebElement lblPageHeading;

  public CreditCardOfferPage() {
    super();
  }

  @Override
  protected void openPage() {
    //Do nothing.
  }

  @Override
  public boolean isPageOpened() {
    try {
      return lblPageHeading.isDisplayed();
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  public String getTextNoButton() {
    return btnNo.getText();
  }

  public FeaturesExpressCheckingPage clickNoButton(){
    btnNo.click();
    return new FeaturesExpressCheckingPage();
  }

  public String getTextYesButton() {
    return btnYes.getText();
  }

  public FeaturesExpressCheckingPage clickYesButton(){
    btnYes.click();
    return new FeaturesExpressCheckingPage();
  }

  public String getTextOfferText(){
    return lblOfferText.getText();
  }

  public String getTextPageHeading() {
    return lblPageHeading.getText();
  }

}
