package com.elyxor.testautomation.pages.account.opening;

import com.elyxor.testautomation.pages.PageBase;
import java.util.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DepositMoneyCreditCardPage extends PageBase {

  private static final Logger logger = LoggerFactory.getLogger(DepositMoneyCreditCardPage.class);

  private static final String BTN_BACK_XPATH = "";
  private static final String BTN_CANCEL_XPATH = "";
  private static final String BTN_CONTINUE_XPATH = "";
  private static final String BTN_SAMEASPRIMARYNO_XPATH = "";
  private static final String BTN_SAMEASPRIMARYYES_XPATH = "";
  private static final String DP_EXPIRATIONDATE_XPATH = "";
  private static final String LBL_DEPOSITSOURCE_XPATH = "";
  private static final String LBL_PAGEHEADING_XPATH = "";
  private static final String TXT_CREDITCARDNUMBER_XPATH = "";
  private static final String TXT_CVV_XPATH = "";
  private static final String TXT_FUNDINGAMOUNT_XPATH = "";

  @FindBy(xpath = BTN_BACK_XPATH)
  @CacheLookup
  WebElement btnBack;

  @FindBy(xpath = BTN_CANCEL_XPATH)
  @CacheLookup
  WebElement btnCancel;

  @FindBy(xpath = BTN_CONTINUE_XPATH)
  @CacheLookup
  WebElement btnContinue;

  @FindBy(xpath = BTN_SAMEASPRIMARYNO_XPATH)
  WebElement btnSameAsPrimaryNo;

  @FindBy(xpath = BTN_SAMEASPRIMARYYES_XPATH)
  WebElement btnSameAsPrimaryYes;

  @FindBy(xpath = DP_EXPIRATIONDATE_XPATH)
  WebElement dpExpirationDate;

  @FindBy(xpath = LBL_DEPOSITSOURCE_XPATH)
  @CacheLookup
  WebElement lblDepositSource;

  @FindBy(xpath = LBL_PAGEHEADING_XPATH)
  @CacheLookup
  WebElement lblPageHeading;

  @FindBy(xpath = TXT_CREDITCARDNUMBER_XPATH)
  WebElement txtCreditCardNumber;

  @FindBy(xpath = TXT_CVV_XPATH)
  WebElement txtCVV;

  @FindBy(xpath = TXT_FUNDINGAMOUNT_XPATH)
  WebElement txtFundingAmount;

  public DepositMoneyCreditCardPage() {
    super();
  }

  @Override
  protected void openPage() {
    //Do nothing.
  }

  @Override
  public boolean isPageOpened() {
    try {
      return lblPageHeading.isDisplayed();
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  public String getTextBackButton() {
    return btnBack.getText();
  }

  public String getTextCancelButton() {
    return btnCancel.getText();
  }

  public String getTextContinueButton() {
    return btnContinue.getText();
  }

  public CreditCardOfferPage clickContinueButton() {
    btnContinue.click();
    return new CreditCardOfferPage();
  }

  public String getTextSameAsPrimaryNoButton(){
    return btnSameAsPrimaryNo.getText();
  }

  public void clickSameAsPrimaryNoButton(){
    btnSameAsPrimaryNo.click();
  }

  public String getTextSameAsPrimaryYesButton(){
    return btnSameAsPrimaryYes.getText();
  }

  public void clickSameAsPrimaryYesButton(){
    btnSameAsPrimaryYes.click();
  }

  public String getTextExpirationDate() {
    return dpExpirationDate.getText();
  }

  public void selectExpirationDate(String date) {
    dpExpirationDate.sendKeys(date);
  }

  public String getTextDepositSource() {
    return lblDepositSource.getText();
  }

  public String getTextPageHeading() {
    return lblPageHeading.getText();
  }

  public String getTextCreditCardNumber(){
    return txtCreditCardNumber.getText();
  }

  public void setTextCreditCardNumber(String number){
    txtCreditCardNumber.sendKeys(number);
  }

  public String getTextCVV(){
    return txtCVV.getText();
  }

  public void setTextCVV(String number){
    txtCVV.sendKeys(number);
  }

  public String getTextFundingAmount(){
    return txtFundingAmount.getText();
  }

  public void setTextFundingAmount(String amount){
    txtFundingAmount.sendKeys(amount);
  }

}
