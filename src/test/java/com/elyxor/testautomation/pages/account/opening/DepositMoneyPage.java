package com.elyxor.testautomation.pages.account.opening;

import com.elyxor.testautomation.pages.PageBase;
import java.util.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DepositMoneyPage extends PageBase {

  private static final Logger logger = LoggerFactory.getLogger(DepositMoneyPage.class);

  private static final String BTN_CREDITCARD_XPATH = "";
  private static final String BTN_DEPOSITCHECK_XPATH = "";
  private static final String BTN_FINDBRANCH_XPATH = "";
  private static final String BTN_MAILCHECK_XPATH = "";
  private static final String BTN_MOVEMONEY_XPATH = "";
  private static final String LBL_PAGEHEADING_XPATH = "";
  private static final String LNK_BACK_XPATH = "";
  private static final String LNK_CANCEL_XPATH = "";

  @FindBy(xpath = BTN_CREDITCARD_XPATH)
  @CacheLookup
  WebElement btnCreditCard;

  @FindBy(xpath = BTN_DEPOSITCHECK_XPATH)
  @CacheLookup
  WebElement btnDepositCheck;

  @FindBy(xpath = BTN_FINDBRANCH_XPATH)
  @CacheLookup
  WebElement btnFindBranch;

  @FindBy(xpath = BTN_MAILCHECK_XPATH)
  @CacheLookup
  WebElement btnMailCheck;

  @FindBy(xpath = BTN_MOVEMONEY_XPATH)
  @CacheLookup
  WebElement btnMoveMoney;

  @FindBy(xpath = LBL_PAGEHEADING_XPATH)
  @CacheLookup
  WebElement lblPageHeading;

  @FindBy(xpath = LNK_BACK_XPATH)
  @CacheLookup
  WebElement lnkBack;

  @FindBy(xpath = LNK_CANCEL_XPATH)
  @CacheLookup
  WebElement lnkCancel;


  public DepositMoneyPage() {
    super();
  }

  @Override
  protected void openPage() {
    //Do nothing.
  }

  @Override
  public boolean isPageOpened() {
    try {
      return lblPageHeading.isDisplayed();
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  public String getTextCreditCardButton(){
    return btnCreditCard.getText();
  }

  public DepositMoneyCreditCardPage clickCreditCardButton(){
    btnCreditCard.click();
    return new DepositMoneyCreditCardPage();
  }

  public String getTextDepositCheckButton(){
    return btnDepositCheck.getText();
  }

  public String getTextFindBranchButton(){
    return btnFindBranch.getText();
  }

  public String getTextMailCheckButton(){
    return btnMailCheck.getText();
  }

  public String getTextMoveMoneyButton(){
    return btnMoveMoney.getText();
  }

  public String getTextPageHeading() {
    return lblPageHeading.getText();
  }

  public String getTextBackLink() {
    return lnkBack.getText();
  }

  public String getTextCancelLink() {
    return lnkCancel.getText();
  }

}
