package com.elyxor.testautomation.pages.account.opening;

import com.elyxor.testautomation.configuration.TestAutomationConfiguration;
import com.elyxor.testautomation.pages.PageBase;
import com.elyxor.testautomation.utils.driver.Driver;
import java.util.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WelcomePage extends PageBase {

  private static final Logger logger = LoggerFactory.getLogger(WelcomePage.class);

  private static final String BTN_OPENNOW_XPATH = "";

  @FindBy(xpath = BTN_OPENNOW_XPATH)
  @CacheLookup
  WebElement btnOpenNow;

  public WelcomePage() {
    super();
  }

  @Override
  protected void openPage() {
    TestAutomationConfiguration configInstance = TestAutomationConfiguration.getInstance();
    String url = String.format("%s", configInstance.getValue("test.account.open.baseurl"));
    logger.info("Welcome Page URL: " + url);
    Driver.getWebDriver().get(url);
  }

  @Override
  public boolean isPageOpened() {
    try {
      return btnOpenNow.isDisplayed();
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  public String getTextOpenNowButton() {
    return btnOpenNow.getText();
  }

  public CustomerTypePage clickOpenNowButton() {
    btnOpenNow.click();
    return new CustomerTypePage();
  }

}
