package com.elyxor.testautomation.pages.account.opening;

import com.elyxor.testautomation.pages.PageBase;
import java.util.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ManualEntryPage extends PageBase {

  private static final Logger logger = LoggerFactory.getLogger(ManualEntryPage.class);

  private static final String BTN_BACK_XPATH = "";
  private static final String BTN_CANCEL_XPATH = "";
  private static final String BTN_CONTINUE_XPATH = "";
  private static final String BTN_GIVECONSENTNO_XPATH = "";
  private static final String BTN_GIVECONSENTYES_XPATH = "";
  private static final String BTN_ISMOBILENO_XPATH = "";
  private static final String BTN_ISMOBILEYES_XPATH = "";
  private static final String LBL_PAGEHEADING_XPATH = "";
  private static final String TXT_EMAILADDRESS_XPATH = "";
  private static final String TXT_NAMEFIRST_XPATH = "";
  private static final String TXT_NAMELAST_XPATH = "";
  private static final String TXT_PRIMARYPHONENUMBER_XPATH = "";

  @FindBy(xpath = BTN_BACK_XPATH)
  @CacheLookup
  WebElement btnBack;

  @FindBy(xpath = BTN_CANCEL_XPATH)
  @CacheLookup
  WebElement btnCancel;

  @FindBy(xpath = BTN_CONTINUE_XPATH)
  @CacheLookup
  WebElement btnContinue;

  @FindBy(xpath = BTN_ISMOBILENO_XPATH)
  WebElement btnIsMobileNo;

  @FindBy(xpath = BTN_ISMOBILEYES_XPATH)
  WebElement btnIsMobileYes;

  @FindBy(xpath = BTN_GIVECONSENTNO_XPATH)
  WebElement btnGiveConsentNo;

  @FindBy(xpath = BTN_GIVECONSENTYES_XPATH)
  WebElement btnGiveConsentYes;

  @FindBy(xpath = LBL_PAGEHEADING_XPATH)
  WebElement lblPageHeading;

  @FindBy(xpath = TXT_EMAILADDRESS_XPATH)
  WebElement txtEmailAddress;

  @FindBy(xpath = TXT_NAMEFIRST_XPATH)
  WebElement txtNameFirst;

  @FindBy(xpath = TXT_NAMELAST_XPATH)
  WebElement txtNameLast;

  @FindBy(xpath = TXT_PRIMARYPHONENUMBER_XPATH)
  WebElement txtPrimaryPhoneNumber;

  public ManualEntryPage() {
    super();
  }

  @Override
  protected void openPage() {
    // Do nothing.
  }

  @Override
  public boolean isPageOpened() {
    try {
      return lblPageHeading.isDisplayed();
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  public String getTextBackButton() {
    return btnBack.getText();
  }

  public String getTextCancelButton() {
    return btnCancel.getText();
  }

  public String getTextContinueButton() {
    return btnContinue.getText();
  }

  public ManualEntryContinuedPage clickContinueButton() {
    btnContinue.click();
    return new ManualEntryContinuedPage();
  }

  public String getTextConsentNo() {
    return btnGiveConsentNo.getText();
  }

  public String getTextConsentYes() {
    return btnGiveConsentYes.getText();
  }

  public String getTextMobilePhoneNo() {
    return btnIsMobileNo.getText();
  }

  public String getTextMobilePhoneYes() {
    return btnIsMobileYes.getText();
  }

  public String getTextPageHeading() {
    return lblPageHeading.getText();
  }

  public String getTextEmailAddress() {
    return txtEmailAddress.getText();
  }

  public void setTextEmailAddress(String emailAdress) {
    txtEmailAddress.sendKeys(emailAdress);
  }

  public String getTextFirstName() {
    return txtNameFirst.getText();
  }

  public void setTextFirstName(String firstName) {
    txtNameFirst.sendKeys(firstName);
  }

  public String getTextLastName() {
    return txtNameLast.getText();
  }

  public void setTextLastName(String lastName) {
    txtNameLast.sendKeys(lastName);
  }

  public String getTextPrimaryPhoneNumber() {
    return txtPrimaryPhoneNumber.getText();
  }

  public void setTextPrimaryPhoneNumber(String phoneNumber) {
    txtPrimaryPhoneNumber.sendKeys(phoneNumber);
  }

}
