package com.elyxor.testautomation.pages.account.opening;

import com.elyxor.testautomation.pages.PageBase;
import java.util.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SignUpMethodPage extends PageBase {

  private static final Logger logger = LoggerFactory.getLogger(SignUpMethodPage.class);

  private static final String BTN_DRIVERSLICENSE_XPATH = "";
  private static final String BTN_FACEBOOK_XPATH = "";
  private static final String BTN_OLDSCHOOL_XPATH = "";
  private static final String BTN_TWITTER_XPATH = "";
  private static final String LBL_PAGEHEADING_XPATH = "";
  private static final String LNK_BACK_XPATH = "";
  private static final String LNK_CANCEL_XPATH = "";

  @FindBy(xpath = BTN_DRIVERSLICENSE_XPATH)
  @CacheLookup
  WebElement btnDriversLicense;

  @FindBy(xpath = BTN_FACEBOOK_XPATH)
  @CacheLookup
  WebElement btnFacebook;

  @FindBy(xpath = BTN_OLDSCHOOL_XPATH)
  @CacheLookup
  WebElement btnOldSchool;

  @FindBy(xpath = BTN_TWITTER_XPATH)
  @CacheLookup
  WebElement btnTwitter;

  @FindBy(xpath = LBL_PAGEHEADING_XPATH)
  @CacheLookup
  WebElement lblPageHeading;

  @FindBy(xpath = LNK_BACK_XPATH)
  @CacheLookup
  WebElement lnkBack;

  @FindBy(xpath = LNK_CANCEL_XPATH)
  @CacheLookup
  WebElement lnkCancel;

  public SignUpMethodPage() {
    super();
  }

  @Override
  protected void openPage() {
    // Do nothing.
  }

  @Override
  public boolean isPageOpened() {
    try {
      return lblPageHeading.isDisplayed();
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  public String getTextDriversLicenseButton() {
    return btnDriversLicense.getText();
  }

  public String getTextFacebookButton() {
    return btnFacebook.getText();
  }

  public String getTextOldSchoolButton() {
    return btnOldSchool.getText();
  }

  public ManualEntryPage clickOldSchoolButton() {
    btnOldSchool.click();
    return new ManualEntryPage();
  }

  public String getTextTwitterButton() {
    return btnTwitter.getText();
  }

  public String getTextPageHeading() {
    return lblPageHeading.getText();
  }

  public String getTextBackLink() {
    return lnkBack.getText();
  }

  public String getTextCancelLink() {
    return lnkCancel.getText();
  }

}
