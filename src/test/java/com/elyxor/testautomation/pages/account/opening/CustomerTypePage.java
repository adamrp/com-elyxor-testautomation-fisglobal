package com.elyxor.testautomation.pages.account.opening;

import com.elyxor.testautomation.pages.PageBase;
import java.util.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CustomerTypePage extends PageBase {

  private static final Logger logger = LoggerFactory.getLogger(CustomerTypePage.class);

  private static final String BTN_CUSTOMERAPPLICATION_XPATH = "";
  private static final String BTN_CUSTOMEREXISTING_XPATH = "";
  private static final String BTN_CUSTOMERNEW_XPATH = "";
  private static final String LBL_PAGEHEADING_XPATH = "";
  private static final String LNK_CANCEL_XPATH = "";

  @FindBy(xpath = BTN_CUSTOMERAPPLICATION_XPATH)
  @CacheLookup
  WebElement btnCustomerApplication;

  @FindBy(xpath = BTN_CUSTOMEREXISTING_XPATH)
  @CacheLookup
  WebElement btnCustomerExisting;

  @FindBy(xpath = BTN_CUSTOMERNEW_XPATH)
  @CacheLookup
  WebElement btnCustomerNew;

  @FindBy(xpath = LBL_PAGEHEADING_XPATH)
  @CacheLookup
  WebElement lblPageHeading;

  @FindBy(xpath = LNK_CANCEL_XPATH)
  @CacheLookup
  WebElement lnkCancel;

  public CustomerTypePage() {
    super();
  }

  @Override
  protected void openPage() {
    //Do nothing.
  }

  @Override
  public boolean isPageOpened() {
    try {
      return lblPageHeading.isDisplayed();
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  public String getTextExistingApplicationButton() {
    return btnCustomerApplication.getText();
  }

  public String getTextExistingCustomerButton() {
    return btnCustomerExisting.getText();
  }

  public String getTextNewCustomerButton() {
    return btnCustomerNew.getText();
  }

  public ProductSelectionPage clickNewCustomerButton() {
    btnCustomerNew.click();
    return new ProductSelectionPage();
  }

  public String getTextPageHeading() {
    return lblPageHeading.getText();
  }

  public String getTextCancelLink() {
    return lnkCancel.getText();
  }

  public void clickCancelLink() {
    lnkCancel.click();
  }

}
