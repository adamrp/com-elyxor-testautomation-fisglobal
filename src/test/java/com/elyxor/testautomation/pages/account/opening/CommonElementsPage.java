package com.elyxor.testautomation.pages.account.opening;

import com.elyxor.testautomation.pages.PageBase;
import java.util.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CommonElementsPage extends PageBase {

  private static final Logger logger = LoggerFactory.getLogger(CommonElementsPage.class);

  private static final String BTN_BACK_XPATH = "";
  private static final String BTN_CANCEL_XPATH = "";
  private static final String BTN_CONTINUE_XPATH = "";
  private static final String IMG_MYCART_XPATH = "";
  private static final String LBL_MYCARTQUANTITY_XPATH = "";
  private static final String LBL_PAGEHEADING_XPATH = "";
  private static final String LNK_BACK_XPATH = "";
  private static final String LNK_CANCEL_XPATH = "";

  @FindBy(xpath = BTN_BACK_XPATH)
  @CacheLookup
  WebElement btnBack;

  @FindBy(xpath = BTN_CANCEL_XPATH)
  @CacheLookup
  WebElement btnCancel;

  @FindBy(xpath = BTN_CONTINUE_XPATH)
  @CacheLookup
  WebElement btnContinue;

  @FindBy(xpath = IMG_MYCART_XPATH)
  @CacheLookup
  WebElement imgMyCart;

  @FindBy(xpath = LBL_MYCARTQUANTITY_XPATH)
  WebElement lblMyCartQuantity;

  @FindBy(xpath = LBL_PAGEHEADING_XPATH)
  @CacheLookup
  WebElement lblPageHeading;

  @FindBy(xpath = LNK_BACK_XPATH)
  @CacheLookup
  WebElement lnkBack;

  @FindBy(xpath = LNK_CANCEL_XPATH)
  @CacheLookup
  WebElement lnkCancel;

  public CommonElementsPage() {
    super();
  }

  @Override
  protected void openPage() {
    //Do nothing.
  }

  @Override
  public boolean isPageOpened() {
    try {
      return lblPageHeading.isDisplayed();
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  public String getTextBackButton() {
    return btnBack.getText();
  }

  public void clickBackButton() {
    btnBack.click();
  }

  public String getTextCancelButton() {
    return btnCancel.getText();
  }

  public void clickCancelButton() {
    btnCancel.click();
  }

  public String getTextContinueButton() {
    return btnContinue.getText();
  }

  public void clickContinueButton() {
    btnContinue.click();
  }

  public String getFilePathMyCartImage() {
    return imgMyCart.getAttribute("src");
  }

  public String getTextMyCartQuantity() {
    return lblMyCartQuantity.getText();
  }

  public String getTextPageHeading() {
    return lblPageHeading.getText();
  }

  public String getTextBackLink(){
    return lnkBack.getText();
  }

  public void clickBackLink(){
    lnkBack.click();
  }

  public String getTextCancelLink(){
    return lnkCancel.getText();
  }

  public void clickCancelLink(){
    lnkCancel.click();
  }

}
