package com.elyxor.testautomation.pages.account.opening;

import com.elyxor.testautomation.pages.PageBase;
import java.util.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TermsAndConditionsPage extends PageBase {

  private static final Logger logger = LoggerFactory.getLogger(TermsAndConditionsPage.class);

  private static final String BTN_BACK_XPATH = "";
  private static final String BTN_CANCEL_XPATH = "";
  private static final String BTN_CONTINUE_XPATH = "";
  private static final String CHK_ACCEPTTERMS_XPATH = "";
  private static final String CHK_ELECTRONICCONSENT_XPATH = "";
  private static final String LBL_PAGEHEADING_XPATH = "";
  private static final String LNK_DOWNLOADACROBAT_XPATH = "";
  private static final String LNK_GENERALACCOUNTDISCLOSURES_XPATH = "";
  private static final String LNK_PRINTANDDOWNLOAD_XPATH = "";

  @FindBy(xpath = BTN_BACK_XPATH)
  @CacheLookup
  WebElement btnBack;

  @FindBy(xpath = BTN_CANCEL_XPATH)
  @CacheLookup
  WebElement btnCancel;

  @FindBy(xpath = BTN_CONTINUE_XPATH)
  @CacheLookup
  WebElement btnContinue;

  @FindBy(xpath = CHK_ACCEPTTERMS_XPATH)
  WebElement chkAcceptTerms;

  @FindBy(xpath = CHK_ELECTRONICCONSENT_XPATH)
  WebElement chkElectronicConsent;

  @FindBy(xpath = LBL_PAGEHEADING_XPATH)
  @CacheLookup
  WebElement lblPageHeading;

  @FindBy(xpath = LNK_DOWNLOADACROBAT_XPATH)
  @CacheLookup
  WebElement lnkDownloadAcrobat;

  @FindBy(xpath = LNK_GENERALACCOUNTDISCLOSURES_XPATH)
  @CacheLookup
  WebElement lnkGeneralAccountDisclosures;

  @FindBy(xpath = LNK_PRINTANDDOWNLOAD_XPATH)
  @CacheLookup
  WebElement lnkPrintAndDownload;


  public TermsAndConditionsPage() {
    super();
  }

  @Override
  protected void openPage() {
    //Do nothing.
  }

  @Override
  public boolean isPageOpened() {
    try {
      return lblPageHeading.isDisplayed();
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  public String getTextBackButton() {
    return btnBack.getText();
  }

  public String getTextCancelButton() {
    return btnCancel.getText();
  }

  public String getTextContinueButton() {
    return btnContinue.getText();
  }

  public DepositMoneyPage clickContinueButton() {
    btnContinue.click();
    return new DepositMoneyPage();
  }

  public void clickAcceptTermsCheckbox(){
    chkAcceptTerms.click();
  }

  public void clickElectronicConsentCheckbox(){
    chkElectronicConsent.click();
  }

  public String getTextPageHeading() {
    return lblPageHeading.getText();
  }

  public String getTextDownloadAcrobatLink(){
    return lnkDownloadAcrobat.getText();
  }

  public String getTextGeneralAccountDisclosuresLink(){
    return lnkGeneralAccountDisclosures.getText();
  }

  public String getTextPrintAndDownloadLink(){
    return lnkPrintAndDownload.getText();
  }

}
