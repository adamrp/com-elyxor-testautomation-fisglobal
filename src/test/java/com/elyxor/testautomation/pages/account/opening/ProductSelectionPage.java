package com.elyxor.testautomation.pages.account.opening;

import com.elyxor.testautomation.pages.PageBase;
import java.util.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProductSelectionPage extends PageBase {

  private static final Logger logger = LoggerFactory.getLogger(ProductSelectionPage.class);

  private static final String BTN_BACK_XPATH = "";
  private static final String BTN_BUSINESS_XPATH = "";
  private static final String BTN_CANCEL_XPATH = "";
  private static final String BTN_CONTINUE_XPATH = "";
  private static final String BTN_DECREACEQUANTITYEXPRESSCHECKING_XPATH = "";
  private static final String BTN_INCREASEQUANTITYEXPRESSCHECKING_XPATH = "";
  private static final String BTN_INDIVIDUAL_XPATH = "";
  private static final String BTN_VERIFYPROMOCODE_XPATH = "";
  private static final String LBL_PAGEHEADING_XPATH = "";
  private static final String LBL_QUANTITYEXPRESSCHECKING_XPATH = "";
  private static final String LBL_ZIPCODE_XPATH = "";
  private static final String LNK_ZIPCODE_XPATH = "";
  private static final String TXT_PROMOCODE_XPATH = "";

  @FindBy(xpath = BTN_BACK_XPATH)
  @CacheLookup
  WebElement btnBack;

  @FindBy(xpath = BTN_BUSINESS_XPATH)
  WebElement btnBusiness;

  @FindBy(xpath = BTN_CANCEL_XPATH)
  @CacheLookup
  WebElement btnCancel;

  @FindBy(xpath = BTN_CONTINUE_XPATH)
  @CacheLookup
  WebElement btnContinue;

  @FindBy(xpath = BTN_DECREACEQUANTITYEXPRESSCHECKING_XPATH)
  @CacheLookup
  WebElement btnDecreaseQuantityExpressChecking;

  @FindBy(xpath = BTN_INCREASEQUANTITYEXPRESSCHECKING_XPATH)
  @CacheLookup
  WebElement btnIncreaseQuantityExpressChecking;

  @FindBy(xpath = BTN_INDIVIDUAL_XPATH)
  WebElement btnIndividual;

  @FindBy(xpath = BTN_VERIFYPROMOCODE_XPATH)
  @CacheLookup
  WebElement btnVerifyPromoCode;

  @FindBy(xpath = LBL_PAGEHEADING_XPATH)
  @CacheLookup
  WebElement lblPageHeading;

  @FindBy(xpath = LBL_QUANTITYEXPRESSCHECKING_XPATH)
  @CacheLookup
  WebElement lblQuantityExpressChecking;

  @FindBy(xpath = LBL_ZIPCODE_XPATH)
  @CacheLookup
  WebElement lblZipCode;

  @FindBy(xpath = LNK_ZIPCODE_XPATH)
  WebElement lnkZipCode;

  @FindBy(xpath = TXT_PROMOCODE_XPATH)
  WebElement txtPromoCode;

  public ProductSelectionPage() {
    super();
  }

  @Override
  protected void openPage() {
    //Do nothing.
  }

  @Override
  public boolean isPageOpened() {
    try {
      return lblPageHeading.isDisplayed();
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  public String getTextBackButton() {
    return btnBack.getText();
  }

  public String getTextBusinessButton(){
    return btnBusiness.getText();
  }

  public void clickBusinessButton(){
    btnBusiness.click();
  }

  public String getTextCancelButton() {
    return btnCancel.getText();
  }

  public String getTextContinueButton() {
    return btnContinue.getText();
  }

  public SignUpMethodPage clickContinueButton() {
    btnContinue.click();
    return new SignUpMethodPage();
  }

  public String getTextExpressCheckingDecreaseQuantityButton() {
    return btnDecreaseQuantityExpressChecking.getText();
  }

  public void clickExpressCheckingDecreaseQuantityButton() {
    btnDecreaseQuantityExpressChecking.click();
  }

  public String getTextExpressCheckingIncreaseQuantityButton() {
    return btnIncreaseQuantityExpressChecking.getText();
  }

  public void clickExpressCheckingIncreaseQuantityButton() {
    btnIncreaseQuantityExpressChecking.click();
  }

  public String getTextIndividualButton(){
    return btnIndividual.getText();
  }

  public void clickIndividualButton(){
    btnIndividual.click();
  }

  public String getTextVerifyPromoCodeButton(){
    return btnVerifyPromoCode.getText();
  }

  public void clickVerifyPromoCodeButton(){
    btnVerifyPromoCode.click();
  }

  public String getTextPageHeading() {
    return lblPageHeading.getText();
  }

  public String getTextExpressCheckingQuantity() {
    return lblQuantityExpressChecking.getText();
  }

  public String getTextZipCodeLabel() {
    return lblZipCode.getText();
  }

  public String getTextZipCodeLink() {
    return lnkZipCode.getText();
  }

  public void clickZipCodeLink() {
    lnkZipCode.click();
  }

  public String getTextPromoCode() {
    return txtPromoCode.getText();
  }

  public void setTextPromoCode(String code) {
    txtPromoCode.sendKeys(code);
  }
}
