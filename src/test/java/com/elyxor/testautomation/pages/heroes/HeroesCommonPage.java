package com.elyxor.testautomation.pages.heroes;

import com.elyxor.testautomation.pages.PageBase;
import java.util.List;
import java.util.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

public class HeroesCommonPage extends PageBase {

  private static final String LBL_TOUROFHEROES_XPATH = "/html/body/app-root/h1";
  private static final String LBL_MESSAGES_XPATH = "/html/body/app-root/app-messages/div/h2";
  private static final String LST_HEROSERVICEMESSAGES_XPATH = "/html/body/app-root/app-messages/div/div[.]";
  private static final String BTN_DASHBOARD_XPATH = "/html/body/app-root/nav/a[1]";
  private static final String BTN_HEROES_XPATH = "/html/body/app-root/nav/a[2]";
  private static final String BTN_HEROESBAD_XPATH = "/html/body/app-root/nav/a[2]";
  private static final String BTN_CLEAR_XPATH = "/html/body/app-root/app-messages/div/button";

  @FindBy(xpath = LBL_TOUROFHEROES_XPATH)
  @CacheLookup
  WebElement lblTourOfHeroes;

  @FindBy(xpath = LBL_MESSAGES_XPATH)
  @CacheLookup
  WebElement lblMessages;

  @FindBy(xpath = LST_HEROSERVICEMESSAGES_XPATH)
  List<WebElement> lstHeroServiceMessages;

  @FindBy(xpath = BTN_DASHBOARD_XPATH)
  @CacheLookup
  WebElement btnDashboard;

  @FindBy(xpath = BTN_HEROES_XPATH)
  @CacheLookup
  WebElement btnHeroes;

  @FindBy(xpath = BTN_HEROESBAD_XPATH)
  @CacheLookup
  WebElement btnHeroesBad;


  @FindBy(xpath = BTN_CLEAR_XPATH)
  @CacheLookup
  WebElement btnClear;

  public HeroesCommonPage() {
    super();
  }

  @Override
  protected void openPage() {
    // do nothing
  }

  @Override
  public boolean isPageOpened() {
    try {
      return lblTourOfHeroes.isDisplayed();
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  public String getTextLblTourOfHeores() {
    return lblTourOfHeroes.getText();
  }

  public String getTextLblMessages() {
    return lblMessages.getText();
  }

  public String getTextLstHeroServiceMessages() {
    return getTextListItem(lstHeroServiceMessages, true);
  }

  public String getTextLstHeroServiceMessage(int itemNumber) {
    return getTextListItem(lstHeroServiceMessages, itemNumber);
  }

  public String getTextLstHeroServiceMessage() {
    return getTextListItem(lstHeroServiceMessages, false);
  }

  public void clickBtnDashboard() {
    btnDashboard.click();
  }

  public String getTextBtnDashboard() {
    return btnDashboard.getText();
  }

  public HeroesHeroesPage clickBtnHeroes() {
    btnHeroes.click();
    return new HeroesHeroesPage();
  }

  public HeroesHeroesPage clickBtnHeroesBad() {
    btnHeroesBad.click();
    return new HeroesHeroesPage();
  }

  public String getTextBtnHeroes() {
    return btnHeroes.getText();
  }

  public void clickBtnClear() {
    btnClear.click();
  }

  public String getTextBtnClear() {
    return btnClear.getText();
  }

}
