package com.elyxor.testautomation.pages.heroes;

import java.util.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

public class HeroesDetailPage extends HeroesCommonPage {

  private static final String LBL_HERODETAILS_XPATH = "/html/body/app-root/app-hero-detail/div/h2";
  private static final String LBL_HEROID_XPATH = "/html/body/app-root/app-hero-detail/div/div[1]/span";
  private static final String LBL_NAME_XPATH = "/html/body/app-root/app-hero-detail/div/div[2]/label";
  private static final String BTN_GOBACK_XPATH = "/html/body/app-root/app-hero-detail/div/button[1]";
  private static final String BTN_SAVE_XPATH = "/html/body/app-root/app-hero-detail/div/button[2]";
  private static final String TXT_HERONAME_XPATH = "/html/body/app-root/app-hero-detail/div/div[2]/label/input";

  @FindBy(xpath = LBL_HERODETAILS_XPATH)
  @CacheLookup
  WebElement lblHeroDetails;

  @FindBy(xpath = LBL_HEROID_XPATH)
  @CacheLookup
  WebElement lblHeroID;

  @FindBy(xpath = LBL_NAME_XPATH)
  @CacheLookup
  WebElement lblName;

  @FindBy(xpath = BTN_GOBACK_XPATH)
  @CacheLookup
  WebElement btnGoBack;

  @FindBy(xpath = BTN_SAVE_XPATH)
  @CacheLookup
  WebElement btnSave;

  @FindBy(xpath = TXT_HERONAME_XPATH)
  @CacheLookup
  WebElement txtHeroName;

  public HeroesDetailPage() {
    super();
  }

  @Override
  protected void openPage() {
    // do nothing
  }

  @Override
  public boolean isPageOpened() {
    try {
      return lblHeroDetails.isDisplayed();
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  public String getTextLblHeroDetails() {
    return lblHeroDetails.getText();
  }

  public String getTextLblHeroId() {
    return lblHeroID.getText();
  }

  public String getTextLblName() {
    return lblName.getText();
  }

  public String getTextBtnGoBack() {
    return btnGoBack.getText();
  }

  public String getTextBtnSave() {
    return btnSave.getText();
  }

  public void clickBtnSave() {
    btnSave.click();
  }

  public void setTextTxtHeroName(String input) {
    txtHeroName.sendKeys(input);
  }

  public String getTextTxtHeroName() {
    return txtHeroName.getAttribute("value");
  }
}
