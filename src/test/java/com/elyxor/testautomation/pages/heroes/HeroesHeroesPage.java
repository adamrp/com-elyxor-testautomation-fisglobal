package com.elyxor.testautomation.pages.heroes;

import java.util.List;
import java.util.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

public class HeroesHeroesPage extends HeroesCommonPage {

  private static final String LBL_MYHEROES_XPATH = "/html/body/app-root/app-heroes/h2";
  private static final String LBL_HERONAME_XPATH = "/html/body/app-root/app-heroes/div/label";
  private static final String LST_HEROESBUTTONS_XPATH = "/html/body/app-root/app-heroes/ul/li[.]/a";
  private static final String LST_REMOVEHEROEBUTTONS_XPATH = "/html/body/app-root/app-heroes/ul/li[.]/button";
  private static final String BTN_ADD_XPATH = "/html/body/app-root/app-heroes/div/button";
  private static final String TXT_HERONAME_XPATH = "/html/body/app-root/app-heroes/div/label/input";

  @FindBy(xpath = LBL_MYHEROES_XPATH)
  @CacheLookup
  WebElement lblMyHeroes;

  @FindBy(xpath = LBL_HERONAME_XPATH)
  @CacheLookup
  WebElement lblHeroName;

  @FindBy(xpath = LST_HEROESBUTTONS_XPATH)
  List<WebElement> lstHeroButtons;

  @FindBy(xpath = LST_REMOVEHEROEBUTTONS_XPATH)
  List<WebElement> lstRemoveHeroButtons;

  @FindBy(xpath = BTN_ADD_XPATH)
  @CacheLookup
  WebElement btnAdd;

  @FindBy(xpath = TXT_HERONAME_XPATH)
  @CacheLookup
  WebElement txtHeroName;

  public HeroesHeroesPage() {
    super();
  }

  @Override
  protected void openPage() {
    // do nothing
  }

  @Override
  public boolean isPageOpened() {
    try {
      return lblMyHeroes.isDisplayed();
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  public String getTextLblMyHeroes() {
    return lblMyHeroes.getText();
  }

  public String getTextLblHeroName() {
    return lblHeroName.getText();
  }

  public String getTextLstHeroes() {
    return getTextListItem(lstHeroButtons, true);
  }

  public String getTextLstHero(int itemNumber) {
    return getTextListItem(lstHeroButtons, itemNumber);
  }

  public String getTextLstHero() {
    return getTextListItem(lstHeroButtons, false);
  }

  public String getTextBtnAdd() {
    return btnAdd.getText();
  }

  public void clickBtnAdd() {
    btnAdd.click();
  }

  public String getTextBtnHeroesList() {
    return getTextListItem(lstRemoveHeroButtons, true);
  }

  public String getTextBtnHeroInList(int itemNumber) {
    return getTextListItem(lstRemoveHeroButtons, itemNumber);
  }

  public String getTextBtnHeroInList() {
    return getTextListItem(lstRemoveHeroButtons, false);
  }

  public void setTextTxtHeroName(String input) {
    txtHeroName.sendKeys(input);
  }

  public String getTextTxtHeroName() {
    return txtHeroName.getAttribute("value");
  }

  public HeroesDetailPage clickBtnHeroInList() {
    clickListItem(lstHeroButtons);
    return new HeroesDetailPage();
  }

  public HeroesDetailPage clickBtnHeroInList(int itemNumber) {
    clickListItem(lstHeroButtons, itemNumber);
    return new HeroesDetailPage();
  }

  public void clickBtnRemoveHeroInList() {
    clickListItem(lstRemoveHeroButtons);
  }

  public void clickBtnRemoveHeroInList(int itemNumber) {
    clickListItem(lstRemoveHeroButtons, itemNumber);
  }
}
