package com.elyxor.testautomation.pages.heroes;

import com.elyxor.testautomation.configuration.TestAutomationConfiguration;
import com.elyxor.testautomation.utils.driver.Driver;
import java.util.List;
import java.util.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HeroesDashboardPage extends HeroesCommonPage {

  private static final Logger logger = LoggerFactory.getLogger(HeroesDashboardPage.class);

  private static final String LBL_TOPHEROES_XPATH = "/html/body/app-root/app-dashboard/h3";
  private static final String LBL_HEROSEARCH_ID = "search-component";
  private static final String LST_TOPHEROES_XPATH = "/html/body/app-root/app-dashboard/div/a[.]/div/h4";
  private static final String LST_HEROSEARCHRESULTS_XPATH = "//*[@id=\"search-component\"]/ul/li[.]";
  private static final String TXT_HEROSEARCH_ID = "search-box";

  @FindBy(xpath = LBL_TOPHEROES_XPATH)
  @CacheLookup
  WebElement lblTopHeroes;

  @FindBy(id = LBL_HEROSEARCH_ID)
  @CacheLookup
  WebElement lblHeroSearch;

  @FindBy(xpath = LST_TOPHEROES_XPATH)
  List<WebElement> lstTopHeroes;

  @FindBy(xpath = LST_HEROSEARCHRESULTS_XPATH)
  List<WebElement> lstHeroSearchResults;

  @FindBy(id = TXT_HEROSEARCH_ID)
  WebElement txtHeroSearch;

  public HeroesDashboardPage() {
    super();
  }

  @Override
  protected void openPage() {
    TestAutomationConfiguration configInstance = TestAutomationConfiguration.getInstance();
    String url = String.format("%s", configInstance.getValue("test.heroes.baseurl"));
    logger.info("Home Page URL: " + url);
    Driver.getWebDriver().get(url);
  }

  @Override
  public boolean isPageOpened() {
    try {
      return lblTopHeroes.isDisplayed();
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  public String getTextLblTopHeroes() {
    return lblTopHeroes.getText();
  }

  public String getTextLblHeroSearch() {
    return lblHeroSearch.getText();
  }

  public String getTextLstTopHeroes(){
    return getTextListItem(lstTopHeroes, true);
  }

  public String getTextLstTopHero(int itemNumber){
    return getTextListItem(lstTopHeroes, itemNumber);
  }

  public String getTextLstTopHero(){
    return getTextListItem(lstTopHeroes, false);
  }

  public HeroesDetailPage clickBtnTopHero(String heroName){
    clickListItem(lstTopHeroes, heroName);
    return new HeroesDetailPage();
  }

  public HeroesDetailPage clickBtnTopHero(int itemNumber){
    clickListItem(lstTopHeroes, itemNumber);
    return new HeroesDetailPage();
  }

  public HeroesDetailPage clickBtnTopHero(){
    clickListItem(lstTopHeroes);
    return new HeroesDetailPage();
  }

  public void setTextHeroSearch(String input) {
    txtHeroSearch.sendKeys(input);
  }

  public String getTextTxtHeroSearch() {
    return txtHeroSearch.getAttribute("value");
  }

  public String getTextBtnHeroSearchResults() {
    return getTextListItem(lstHeroSearchResults, true);
  }

  public HeroesDetailPage clickBtnHeroInSearchResults(String heroName) {
    clickListItem(lstHeroSearchResults, heroName);
    return new HeroesDetailPage();
  }
}
