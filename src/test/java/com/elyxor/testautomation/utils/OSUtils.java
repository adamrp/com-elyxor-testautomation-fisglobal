package com.elyxor.testautomation.utils;

public enum OSUtils {
    WINDOWS, LINUX, MAC, SOLARIS;

    public static OSUtils getOS() {
        OSUtils os = null;
        String operSys = System.getProperty("os.name").toLowerCase();
        if (operSys.contains("win")) {
            os = OSUtils.WINDOWS;
        } else if (operSys.contains("nix") || operSys.contains("nux")
                || operSys.contains("aix")) {
            os = OSUtils.LINUX;
        } else if (operSys.contains("mac")) {
            os = OSUtils.MAC;
        } else if (operSys.contains("sunos")) {
            os = OSUtils.SOLARIS;
        }
        return os;
    }
}
