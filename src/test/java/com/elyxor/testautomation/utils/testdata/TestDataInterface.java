package com.elyxor.testautomation.utils.testdata;

public interface TestDataInterface {

    public String get(String testCase, String key);

    public void printTestCaseData(String testCase);
}
