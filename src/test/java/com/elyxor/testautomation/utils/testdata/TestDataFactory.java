package com.elyxor.testautomation.utils.testdata;


import com.elyxor.testautomation.configuration.TestAutomationConfiguration;
import org.apache.commons.lang3.exception.ContextedRuntimeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestDataFactory {

    private static Logger logger = LoggerFactory.getLogger(TestDataFactory.class);

    public TestData getTestData() {

        // determine the data source from the configuration service
        // and return the a populated TestData object

        TestAutomationConfiguration configInstance = TestAutomationConfiguration.getInstance();
        String testdataType = String.format("%s", configInstance.getValue("test.data.type"));

        switch (testdataType)  {

            case "json":
                try {
                    String testdataFileLocation = String.format("%s", configInstance.getValue("test.data.json.filelocation"));
                    logger.info("Loading data from: " + testdataFileLocation);
                    return new TestDataJson(testdataFileLocation);
                }
                catch (Exception e) {
                    throw new ContextedRuntimeException(String.format("Invalid testdataType: %s", testdataType));
                }

            default:
                throw new ContextedRuntimeException(String.format("Invalid testdataType: %s", testdataType));

        }

    }
}