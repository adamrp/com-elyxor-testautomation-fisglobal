package com.elyxor.testautomation.utils.driver;

public enum Browser {
    CHROME("chrome");   // this enum list is what is supported in the factory

    private String browserName;

    Browser(String browserName) { this.browserName = browserName; }

    public String getBrowserName() {
        return browserName;
    }

    public static Browser getByName(String name){
        for(Browser browser : values()) {
            if(browser.getBrowserName().equalsIgnoreCase(name)) {
                return browser;
            }
        }
        return null;
    }

}
