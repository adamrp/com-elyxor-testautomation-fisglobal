package com.elyxor.testautomation.utils.driver;

import java.util.concurrent.TimeUnit;

import com.elyxor.testautomation.configuration.ConfigurationSource;
import com.elyxor.testautomation.utils.OSUtils;
import org.apache.commons.lang3.exception.ContextedRuntimeException;

import com.elyxor.testautomation.configuration.TestAutomationConfiguration;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WebDriverFactory {

    private static final Logger logger = LoggerFactory.getLogger(WebDriverFactory.class);

    public Driver getDriver(Browser browser) {

        WebDriver webDriver;
        ConfigurationSource configInstance = TestAutomationConfiguration.getInstance();

        logger.info("Getting WebDriver for browser: " + browser);

        switch (browser) {

            case CHROME:

                ChromeOptions chromeOptions = new ChromeOptions();
                String webdriverchromedriver;
                String browserchromelocation = null;

                //******************************************************************
                // determine OSUtils so that use the right Chrome Driver
                // https://sites.google.com/a/chromium.org/chromedriver/downloads
                //******************************************************************
                switch (OSUtils.getOS()) {
                    case WINDOWS:
                        webdriverchromedriver = String.format("%s", configInstance.getValue("webdriver.chrome.driver.win32"));
                        break;
                    case MAC:
                        webdriverchromedriver = String.format("%s", configInstance.getValue("webdriver.chrome.driver.mac64"));
                        break;
                    case LINUX:
                        webdriverchromedriver = String.format("%s", configInstance.getValue("webdriver.chrome.driver.linux64"));
                        break;
                    default:
                        throw new ContextedRuntimeException(String.format("OSUtils not supported"));
                }
                System.setProperty("webdriver.chrome.driver", webdriverchromedriver);
                logger.info("webdriverchromedriver: " + webdriverchromedriver);

                //******************************************************************
                // needed when run in Docker
                //******************************************************************
                chromeOptions.addArguments("--no-sandbox");

                //******************************************************************
                // get optional overrides
                //******************************************************************

                String headless = String.format( "%s",configInstance.getValue("webdriver.chrome.headless") );
                logger.info("headless: " + headless);

                if ((headless.toLowerCase().equals("true")) || (OSUtils.getOS() == OSUtils.LINUX)) {
                    chromeOptions.setHeadless(Boolean.TRUE);
                }

                // use this if Chrome in default locations
                browserchromelocation = String.format( "%s", configInstance.getValue("webdriver.chrome.location") );
                logger.info("browserchromelocation: " + browserchromelocation);

                if (browserchromelocation == null) {
                    chromeOptions.setBinary(browserchromelocation);
                }

                //******************************************************************
                // create the driver instance
                //******************************************************************
                webDriver = new ChromeDriver(chromeOptions);

                //******************************************************************
                // override the default driver behavior
                // can only set once the Driver object exists
                //******************************************************************
                // full screen
                String fullscreen = String.format( "%s",configInstance.getValue("webdriver.window.fullscreen") );
                if (fullscreen.toLowerCase().equals("true")) {
                    webDriver.manage().window().fullscreen();
                }

                // implicit wait
                Integer implicitwait = Integer.parseInt(configInstance.getValue("webdriver.implicitwait"));
                if (implicitwait != null) {
                    webDriver.manage().timeouts().implicitlyWait(implicitwait, TimeUnit.SECONDS);
                }

                return new Driver(webDriver);

            default:
                throw new ContextedRuntimeException(String.format("Browser type unsupported: %s", browser));

        }
    }

}
