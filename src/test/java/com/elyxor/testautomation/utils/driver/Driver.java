package com.elyxor.testautomation.utils.driver;

import com.elyxor.testautomation.configuration.TestAutomationConfiguration;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

public class Driver {

    private static WebDriver webDriver;
    private static final Logger logger = LoggerFactory.getLogger(Driver.class);

    public Driver(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    public static WebDriver getWebDriver() {
        if (webDriver != null) {
            return webDriver;
        } else {
            throw new IllegalStateException("Driver has not been initialized.");
        }
    }

    public void closeDriver(){
        if (webDriver != null) {
            logger.info("Closing Browser");
            webDriver.close();
            webDriver.quit();
        }
        else {
            logger.info("No Browser to close.");
        }
    }

    public String screenshot(String filename) {

        TestAutomationConfiguration configInstance = TestAutomationConfiguration.getInstance();
        String testScreenshotDir = String.format("%s", configInstance.getValue("webdriver.screenshot.dir"));

        File source;
        source = ((TakesScreenshot)getWebDriver()).getScreenshotAs(OutputType.FILE);
        String path = String.format("%s/%s.png", testScreenshotDir, filename);
        try {
            FileUtils.copyFile(source, new File(path));
        }
        catch(IOException e) {
            path = "Failed to capture screenshot: " + e.getMessage();
        }
        return path;
    }
}